# README

## Dependências

Recomendamos o seguinte [link](https://gorails.com/setup/ubuntu/16.04) passo a passo para a instalação.

Esse projeto possui as seguintes depedências:

Sistema Operacional:
* Linux

Linguagem: 
* Ruby 2.3

Framework: 
* Rails 5.1

Banco de dados: 
* Mysql

Servidor de Email: 
* Mailcatcher

## Execução

Na pasta do projeto, execute os seguintes comandos:

* `bundle install`
* `rake db:create`
* `rake db:migrate` 
* `rake db:seed`
* `rails s`
Em outro terminal execute o comando:
* `mailcatcher`