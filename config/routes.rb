Rails.application.routes.draw do
	resources :addresses
	resources :prices
	resources :events
	resources :companies
	resources :addresses

	devise_for :users, :controllers => { 
		:invitations => 'users/invitations'
	}

	resources :users, only: [:index, :destroy] 
	
	get "/users/:id/inviteUser" => "users#invite_User", :as => :invite_User

	resources :events, only: [:create] do
		get :subscribe
	end

	resources :events, only: [:destroy] do
		get :unsubscribe
	end



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "events#index"
end
