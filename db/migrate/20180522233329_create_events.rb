class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.string :contact
      t.datetime :dt_start
      t.datetime :dt_end
      t.references :category, foreign_key: true
      t.references :company, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
