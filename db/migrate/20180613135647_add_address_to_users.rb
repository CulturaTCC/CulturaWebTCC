class AddAddressToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_reference :users, :address, foreign_key: true, on_delete: :cascade
  end
end
