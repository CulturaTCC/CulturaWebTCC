class AddUserToCompany < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :company, foreign_key: true, on_delete: :cascade
  end
end