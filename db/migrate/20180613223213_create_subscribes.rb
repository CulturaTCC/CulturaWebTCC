class CreateSubscribes < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribes do |t|
      t.references :user, foreign_key: true, on_delete: :cascade
      t.references :event, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
