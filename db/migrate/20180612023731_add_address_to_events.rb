class AddAddressToEvents < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :address, foreign_key: true, on_delete: :cascade
  end
end
