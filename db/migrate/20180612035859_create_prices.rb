class CreatePrices < ActiveRecord::Migration[5.1]
  def change
    create_table :prices do |t|
      t.string :title
      t.float :price
      t.references :event, foreign_key: true, on_delete: :cascade

      t.timestamps
    end
  end
end
