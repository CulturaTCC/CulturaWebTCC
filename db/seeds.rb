# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



category_list = [
	"Music",
	"Theater",
	"Cinema",
	"Gastronomy",
	"Architecture",
	"Dance",
	"Visual art",
	"Education",
	"Congress"
]
category_list.each do |name|
	Category.create(name: name)

	User.create(name:"Julio Eduardo Maistrovicz Correa",
		password:"123456", 
		password_confirmation: "123456",
		cpf:"08571573964",
		bio:"é nois que voa bichão",
		email:"julio@hotmail.com")
end