class Company < ApplicationRecord
	#relaçoes
	has_many :users
	belongs_to :address, required: false

	#validaçoes
	validates :name, presence:true, length: { in: 2..50 }
 	validates :cnpj, :cnpj => true, uniqueness: true, :allow_blank => true
end

