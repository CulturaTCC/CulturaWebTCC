class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :timeoutable,
  :timeout_in => 1.hour


  
  #relaçoes
  belongs_to :company, required: false
  belongs_to :address, required: false

      #relação cliente => eventos
      has_many :subscribes
      has_many :events, through: :subscribes

  #validaçoes
  validates :name, presence:true, length: { in: 2..100 }
  validates :cpf, :cpf => true, presence:true

  
  def self.all_users(company, user)
    User.all.where("company_id = ? AND id != ?", company, user)
  end

  def isAdmin?
    if profile == 3
      true
    else
      false
    end
  end 

end
