class Address < ApplicationRecord

	#relacionamento
	has_one :event



	#geolocalização
	geocoded_by :full_address   # can also be an IP address

	reverse_geocoded_by :latitude, :longitude do |obj,results|
		if geo = results.first
			obj.street = geo.address.to_s.split(",")[0]
			obj.city = geo.city
			obj.state = geo.state
			obj.zip = geo.postal_code
		end
	end		 	
	after_validation :geocode, :reverse_geocode, if: ->(obj){ obj.present? and obj.address_changed? }

	

	def self.closest_by_event(find_address)
		addresses = Address.where(id: Event.all.to_a.pluck(:address_id).reject(&:blank?))
		addresses.where("id != ?", find_address)
	end

	def full_address
		[street, city, zip, state].compact.join(",")  
	end

	def address_changed?
		:street_changed? || :city_changed? || :zip_changed? || :state_changed?
	end

end
