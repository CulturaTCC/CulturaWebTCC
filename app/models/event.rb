class Event < ApplicationRecord
  scope :past_events, -> time {where("dt_end < ?", time).order("dt_end ASC")}
  scope :search_name, -> name {where("name like ?", )}
  #relaçoes
  belongs_to :category
  belongs_to :company
  has_many :prices, dependent: :delete_all
  belongs_to :address, dependent: :destroy

    #relação cliente => evento
    has_many :subscribes, dependent: :delete_all
    has_many :users, through: :subscribes 

  #validaçoes
  validates :title, presence:true, length: { in: 2..50 }
  validates :description, presence:true 
  validates :dt_start, presence:true
  validates :dt_end, presence:true, :if => :dt_event?

  #imagem uploader
  mount_uploader :image, ImageUploader

  def dt_event?
    if dt_end.nil? && dt_start.nil?
      dt_end >= dt_start 
    end
  end

  def self.near_search(query)
    address.near(query)
  end


end


