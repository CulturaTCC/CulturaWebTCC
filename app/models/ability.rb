class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities


    can :read, Event # Pessoas nao logadas
    cannot :create, Company
    return unless user.present? # Pessoas logadas
    can :manage, User, :id => user.id
    can :subscribe, Event
    can :unsubscribe, Event
    
    case user.profile
        when 1 # Participante
            can :create, Company
            can :read, Event
        when 2 # Organizador
            can :manage, Event, :company_id => user.company_id
            can :read, Company
            cannot :invite, User
            cannot [:destroy, :update, :create], Company
        when 3 # Gerente
            can :invite, User
            can :manage, Event, :company_id => user.company_id
            can [:read, :update, :destroy], Company, :id => user.company_id
        end
    end
end
