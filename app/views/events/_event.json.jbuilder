json.extract! event, :id, :title, :description, :contact, :dt_start, :dt_end, :category_id, :company_id, :created_at, :updated_at
json.url event_url(event, format: :json)
