// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.mask
//= require jquery_ujs
//= require moment.js
//= require moment/pt-br.js
//= require bootstrap-datetimepicker
//= require bootstrap-sprockets
//= require_tree .


$("[data-mask]").each(function() {
	$(this.value).mask($(this).data("mask"), 
	{
		placeholder: $(this).data("placeholder"),
		reverse: true
	});
});


$(function () {
	$('#datetimepicker1').datetimepicker({
		minDate: 'moment',
		locale: 'pt-br'
	});
});

$(function () {
	$('#datetimepicker2').datetimepicker({
		minDate: 'moment',
		locale: 'pt-br'
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#img_prev')
			.attr('src', e.target.result)
			.width(600)
			.height(400);
		};

		reader.readAsDataURL(input.files[0]);
	}
}