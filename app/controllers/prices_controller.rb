class PricesController < ApplicationController
  before_action :set_price, only: [:show, :edit, :update, :destroy]
  before_action :set_event
  # GET /prices
  # GET /prices.json
  def index
    @prices = Price.all
  end

  # GET /prices/1
  # GET /prices/1.json
  def show
  end

  # GET /prices/new
  def new
    @price = Price.new
    @price.event_id = params[:event_id]
  end

  # GET /prices/1/edit
  def edit
  end

  # POST /prices
  # POST /prices.json
  def create
    @price = Price.new(price_params)
    if @price.title.blank?
      @price.title = "Free"
    end
    respond_to do |format|
      if @price.save
       format.html { redirect_to edit_event_path(Event.find(price_params[:event_id])), notice: 'Price was successfully created.' }
       format.json { render :edit, status: :created, location: event }
     else
      format.html { render :show }
      format.json { render json: @price.errors, status: :unprocessable_entity }
    end
  end
end

  # PATCH/PUT /prices/1
  # PATCH/PUT /prices/1.json
  def update
    respond_to do |format|
binding.pry
      if @price.update(price_params)
        format.html { redirect_to edit_event_path(Event.find(price_params[:event_id])), notice: 'Price was successfully updated.' }
        format.json { render :edit, status: :ok, location: @price }
      else
        format.html { render :edit }
        format.json { render json: @price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prices/1
  # DELETE /prices/1.json
  def destroy
    binding.pry
    event = Event.find(@price.event_id)
    @price.destroy
    respond_to do |format|
      format.html { redirect_to edit_event_path(event), notice: 'Price was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_price
      @price = Price.find_by(params[:event_id]) || Price.new
    end

    def set_event
      @event = Event.find_by_id(params[:event_id]) 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def price_params
      params.require(:price).permit(:title, :price, :event_id)
    end
  end
