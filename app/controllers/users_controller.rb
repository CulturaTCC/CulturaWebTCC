class UsersController < ApplicationController

	def index
		@eventinhos = User.find(1)
	end
	
	def destroy
		
	end

	def invite_User
		if params[:email].nil?
			userInvite = User.find(params[:id])
		else
			userInvite = User.find(email: params[:email])
		end
		respond_to do |format|
			if userInvite.invite!(current_user)
				format.html { redirect_to current_user.company, notice: 'User invitede with sucess.' }
				format.json { render :show, status: :ok, location: current_user.company }
			else
				format.html { render :new }
				format.json { render json: userInvite.errors, status: :unprocessable_entity }  
			end
		end

	end

	private

	def user_params
		params.require(:user).permit(:id, :email)
	end
end