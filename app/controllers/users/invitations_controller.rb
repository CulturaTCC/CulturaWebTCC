class Users::InvitationsController < Devise::InvitationsController
	before_action :initial_invited_user, only: [:new, :edit, :update]

	def new
		if cannot?( :invite, User )
			raise CanCan::AccessDenied
		else
			render '/users/invitations/new', locals: { resource: @user, resource_name: resource_name }
		end
	end

	def edit
		render '/users/invitations/edit', locals: { resource: @user, resource_name: resource_name }
	end

	def create
		if User.find_by_email(invited_user_params[:email]) 
			@user =	User.find_by_email(invited_user_params[:email])
		else
			@user = User.new(invited_user_params) 	
		end
		@user.invite!
		@user.company = current_user.company
		@user.save
		if @user.valid?
			redirect_to current_user.company , notice: "User #{invited_user_params[:email]} has been invited."
		else
			render '/users/invitations/new', locals: { resource: @user, resource_name: resource_name }
		end
	end

	def update
		user = User.accept_invitation!(:invitation_token => params[:invitation_token], :password => params[:password])
		user.profile = 2
		user.save
		if user.valid?
			redirect_to root_path, notice: ''
		else
			super
		end
	end

	private

	def initial_invited_user
		@user = User.find_by(invitation_token: params[:invitation_token]) || User.new
		@user.invitation_token = params[:invitation_token]
		@user
	end

	def invited_user_params
		params.require(:user).permit(:name, :email, :cpf, :password, :password_confirmation, :invitation_token)
	end


end

