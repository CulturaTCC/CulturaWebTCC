class EventsController < ApplicationController
  load_and_authorize_resource
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :set_categories
  before_action :set_price

  # GET /events
  # GET /events.json
  def index

    if params[:time] == "Today"
      @events = Event.where(dt_start.local == Time.zone.today)

      binding.pry
    else
      @events = Event.all

    end
    @carouselEvents = Event.all.limit(3)
  end

  # GET /events/1
  # GET /events/1.json
  def show
    #binding.pry
    @prices = Price.where(event_id: @event.id)
    allEventsAddress = Address.closest_by_event(@event.address_id)
    nearAddresses = allEventsAddress.near(@event.address.street)
    @nearEvents = Event.where(address_id: nearAddresses.to_a.pluck(:id)).all.limit(3)
  end

  # GET /events/new
  def new
    @event = Event.new
    @address = Address.new
    @categories = Category.all
  end

  # GET /events/1/edit
  def edit
    @categories = Category.all
    @address = Event.find(params[:id]).address
  end

  # POST /events
  # POST /events.json
  def create

    @address = Address.new(address_params)
    @event = Event.new(event_params)
    @event.company_id = current_user.company_id

    respond_to do |format|
      if @address.save
        @event.address_id = @address.id
        if @event.save
          format.html { redirect_to edit_event_path(@event), notice: 'Event was successfully created.' }
          format.json { render :edit, status: :created, location: @event }
        else
          format.html { render :new }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }  
      end
    end
  end
  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        if @event.address.update(address_params)
          format.html { redirect_to @event, notice: 'Event was successfully updated.' }
          format.json { render :show, status: :ok, location: @event }
        else
          format.html { render :edit }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
        
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def subscribe
    Subscribe.create(user_id: current_user.id, event_id: params[:event_id])
    respond_to do |format|
      format.html { redirect_to Event.find(params[:event_id]), notice: 'Subscribed in the event.' }
      format.json { head :no_content }
    end
  end

  def unsubscribe
    Subscribe.destroy(Subscribe.where(user_id: current_user.id, event_id: params[:event_id]).ids)
    respond_to do |format|
      format.html { redirect_to Event.find(params[:event_id]), notice: 'Unsubscribed in the event.' }
      format.json { head :no_content }
    end
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def set_price
      @price = Price.find_by_event_id(params[:id]) || Price.new
    end
    
    def set_categories
      @categories = Category.all
    end

    def address_params
      params.require(:address).permit(:street, :city, :zip, :state, :latitude, :longitude)
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :contact, :dt_start, :dt_end, :category_id, 
        :company_id, :image, :time, address: [:street, :city, :zip, :state, :latitude, :longitude])
    end
  end


