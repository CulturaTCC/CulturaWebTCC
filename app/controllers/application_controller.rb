class ApplicationController < ActionController::Base
	before_action :configure_permitted_parameters, if: :devise_controller?
	protect_from_forgery with: :exception




	rescue_from CanCan::AccessDenied do |exception|
		redirect_to '/', :alert => exception.message
	end

	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :cpf, :bio, :profile])
		devise_parameter_sanitizer.permit(:account_update, keys: [:name, :bio, :profile])
	end
end
